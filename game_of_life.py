import random
import sys
import time

from console_menus import *

DEBUG = False

def load_board_state(filename):

	loaded_board = []

	with open(filename) as file:
		for line in file:
			row = []
			for char in line[:-1]:
				print char
				if int(char) == 1:
					row.append(1)
				else:
					row.append(0)

			loaded_board.append(row)

	return loaded_board

def dead_state(width, height):

	if DEBUG:
		print "width = " + str(width)
		print "height = " + str(height)

	board = []
	for y in range(height):
		row = []
		for x in range(width):
			row.append(0)

		board.append(row)

	if DEBUG:
		print board

	return board

def random_state(width, height):

	if DEBUG:
		print "width = " + str(width)
		print "height = " + str(height)

	state = dead_state(width, height)

	for y in range(height):
		for x in range(width):
			rnd = random.random()

			if rnd >= 0.5:
				cell_state = 0
			else:
				cell_state = 1

			state[y][x] = cell_state

	if DEBUG:
		print board

	return state

def next_board_state(board):

	height = len(board)
	width = len(board[0])

	next_state = dead_state(width, height)

	if DEBUG:
		print "next state at next_board_state = "
		print next_state
		render(next_state)

	for y in range(height):
		for x in range(width):
			live_count = 0

			if DEBUG:
				print "x = " + str(x)
				print "y = " + str(y)

			cell = board[y][x]
			
			if x > 0:
				if board[y][x-1] == 1:
					live_count+=1			
			if x < width-1:
				if board[y][x+1] == 1:
					live_count+=1
			if y > 0:
				if board[y-1][x] == 1:
					live_count+=1
			if y < height-1:
				if board[y+1][x] == 1:
					live_count+=1
			
			if x >= 0 and x < width-1 and y < height-1:
				if board[y+1][x+1] == 1:
					live_count+=1
			if x <= width-1 and x > 0 and y < height-1:
				if board[y+1][x-1] == 1:
					live_count+=1
			if y > 0 and y <= height-1 and x < width-1:
				if board[y-1][x+1] == 1:
					live_count+=1
			if y <= height-1 and y > 0 and x > 0:
				if board[y-1][x-1] == 1:
					live_count+=1
					
			new_cell = update_cell(cell, live_count)

			if DEBUG:
				print "new cell = " + str(new_cell)

			next_state[y][x] = new_cell
	return next_state

def update_cell(cell, neighbour_count):
	if cell == 1:
		if neighbour_count <= 1:
			return 0
		elif neighbour_count > 1 and neighbour_count <= 3:
			return 1
		elif neighbour_count > 3:
			return 0
	else:
		if neighbour_count == 3:
			return 1
		else:
			return 0

def render(board):

	height = len(board)
	width = len(board[0])

	print "-" * ((width * 2) + 3)

	for y in range(height):
		row = board[y]
		print "|",
		for cell in row:
			if cell == 0:
				print " ",
			else:
				print "#",

		print "|"

	print "-" * ((width * 2) + 3)

def run(board, count):

	next_state = next_board_state(board)
	render(next_state)

	while count > 0:
		try:
			next_state = next_board_state(next_state)
			render(next_state)
			time.sleep(.200)
			count-=1
		except KeyboardInterrupt:
			break

if __name__=="__main__":

	# defaults

	width = 10
	height = 10
	iteration_count = 200

	if len(sys.argv) == 3 and sys.argv[2] == "DEBUG":
		DEBUG = True

	choice = -1
	basedir = "gol_patterns/"
	
	while choice != 9:
		choice = main_menu()

		if choice == 1:
			board = load_board_state(basedir + "toad.txt")
			run(board, iteration_count)
		if choice == 2:
			board = load_board_state(basedir + "glider.txt")
			run(board, iteration_count)
		if choice == 3:
			board = load_board_state(basedir + "blink.txt")
			run(board, iteration_count)
		if choice == 4:
			board = load_board_state(basedir + "pulsar.txt")
			run(board, iteration_count)
		if choice == 5:
			board = load_board_state(basedir + "glider_gun.txt")
			run(board, iteration_count)
		if choice == 6:
			board = random_state(width, height)
			run(board, iteration_count)
		if choice == 7:
			width, height = get_board_size()
		if choice == 8:
			iteration_count = get_iteration_count()

	sys.exit(0)
