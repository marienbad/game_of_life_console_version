from game_of_life import next_board_state, render

def test_all_dead():
    # TEST : dead cells with no live neighbors
    # should stay dead.
    init_state = [
        [0,0,0],
        [0,0,0],
        [0,0,0]
    ]
    expected_next_state = [
        [0,0,0],
        [0,0,0],
        [0,0,0]
    ]

    actual_next_state = next_board_state(init_state)

    show_results("All dead", expected_next_state, actual_next_state)

def test_exactly_1():
    # TEST : cell with exactly one neighbour
    # should die.
    init_state = [
        [0,0,0],
        [1,1,0],
        [0,0,0]
    ]
    expected_next_state = [
        [0,0,0],
        [0,0,0],
        [0,0,0]
    ]
    actual_next_state = next_board_state(init_state)

    show_results("test exactly 1", expected_next_state, actual_next_state)

def test_exactly_2():
    # TEST : cell with exactly two neighbours
    # should stay alive.
    init_state = [
        [0,0,0],
        [1,1,1],
        [0,0,0]
    ]
    expected_next_state = [
        [0,1,0],
        [0,1,0],
        [0,1,0]
    ]
    actual_next_state = next_board_state(init_state)

    show_results("test exactly 2", expected_next_state, actual_next_state)

def test_exactly_3():
    # TEST : dead cells with exactly 3 neighbors
    # should come alive.
    init_state = [
        [0,0,1],
        [0,1,1],
        [0,0,0]
    ]
    expected_next_state = [
        [0,1,1],
        [0,1,1],
        [0,0,0]
    ]
    actual_next_state = next_board_state(init_state)

    show_results("test exactly 3", expected_next_state, actual_next_state)

def test_more_than_3():
    # TEST : live cells with more than 3 neighbors
    # should die.
    init_state = [
        [1,1,1],
        [0,1,1],
        [0,0,0]
    ]
    expected_next_state = [
        [1,0,1],
        [1,0,1],
        [0,0,0]
    ]
    actual_next_state = next_board_state(init_state)

    show_results("test more than 3", expected_next_state, actual_next_state)

def test_corners():
    # TEST : dead corner cells with exactly 3 neighbors
    # should come alive.

    init_state = [
        [0,1,0],
        [1,1,0],
        [0,0,0]
    ]

    expected_next_state = [
        [1,1,0],
        [1,1,0],
        [0,0,0]
    ]
    actual_next_state = next_board_state(init_state)

    show_results("top left corner", expected_next_state, actual_next_state)

    init_state = [
        [0,1,0],
        [0,1,1],
        [0,0,0]
    ]

    expected_next_state = [
        [0,1,1],
        [0,1,1],
        [0,0,0]
    ]
    actual_next_state = next_board_state(init_state)

    show_results("top right corner", expected_next_state, actual_next_state)

    init_state = [
        [0,0,0],
        [1,1,0],
        [0,1,0]
    ]

    expected_next_state = [
        [0,0,0],
        [1,1,0],
        [1,1,0]
    ]
    actual_next_state = next_board_state(init_state)

    show_results("bottom left corner", expected_next_state, actual_next_state)

    init_state = [
        [0,0,0],
        [0,1,1],
        [0,1,0]
    ]

    expected_next_state = [
        [0,0,0],
        [0,1,1],
        [0,1,1]
    ]
    actual_next_state = next_board_state(init_state)

    show_results("bottom right corner", expected_next_state, actual_next_state)

def show_results(test_name, expected_next_state, actual_next_state):

  if expected_next_state == actual_next_state:
      print "PASSED " + test_name
  else:
	  print "FAILED " + test_name
	  print "Expected:"
	  print expected_next_state
	  render(expected_next_state)
	  print "Actual:"
	  print actual_next_state
	  render(actual_next_state)

if __name__=="__main__":
    test_all_dead()
    test_exactly_1()
    test_exactly_2()
    test_exactly_3()
    test_more_than_3()
    test_corners()
