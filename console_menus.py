
def main_menu():

	while True:
		print "******** Main Menu ********"
		print
		print "1) Run Toad Board"
		print "2) Run Glider Board"
		print "3) Run Blinker Board"
		print "4) Run Pulsar Board"
		print "5) Run Glider Gun Board"
		print "6) Run Random Board"
		print "7) Set Size For Random Board"
		print "8) Set Iterations"
		print "9) Exit Program"
		print
		print "Note: use ctrl-c to stop game"
		print "while it it running the board."
		print
		choice = raw_input("Enter your choice: ")
		print

		try:
			choice = int(choice)
		except ValueError:
			print "Invalid Choice"
			continue

		if choice in [1,2,3,4,5,6,7,8,9]:
			return choice

def get_board_size():
	width = 0
	height = 0

	while True:

		print "******** Enter Size ********"
		print
		width = raw_input("Width (max 20): ")
		height = raw_input("Height (max 20): ")

		try:
			width = int(width)
			height = int(height)
		except ValueError:
			print "Invalid input"
			print
			continue

		if width > 0 and width <= 20 and height > 0 and height <= 20:
			return width, height
		else:
			if width < 0 or height < 0:
				print "Too Small, Please Retry"
				continue
			if width > 20 or height > 20:
				print "Too Big, Please Retry"
				continue

def get_iteration_count():
	count = 0

	while True:

		print "******** Enter Iterations ********"
		print
		count = raw_input("Amt Iterations (max 20000): ")

		try:
			count = int(count)
		except ValueError:
			print "Invalid Amount"
			continue

		if count > 0 and count <= 20000:
			return count
		elif count <= 0:
			print "Must be greater than 0"
			print "Please retry"
		else:
			print "Must be less than 20000"
			print "Please retry"

